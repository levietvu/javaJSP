package JavaServerletJSP;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class ServerletBase extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ServerletBase(){
		super();
	}
	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException;
	protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException;
}
